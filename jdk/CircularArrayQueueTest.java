package jdk;

import static org.junit.jupiter.api.Assertions.*;

public class CircularArrayQueueTest {
    public static void main(String[] args) {
        CircularArrayQueue queue = new CircularArrayQueue();
        System.out.println(queue.isEmpty());
        queue.enqueue("20182327");
        queue.enqueue("wows");
        queue.enqueue("��");
        queue.enqueue("Զ");
        queue.enqueue("ϲ");
        queue.enqueue("��");
        queue.enqueue("YA");
        queue.enqueue("MA");
        queue.enqueue("TO");
        queue.enqueue("!");
        queue.enqueue("!");
        queue.dequeue();

        System.out.println(queue.first());
        System.out.println(queue.toString());
    }
}