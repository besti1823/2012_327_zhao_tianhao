package jdk;

public class complex {
    private double RealPart;
    private double ImagePart;

    public complex(double R,double I){
        this.RealPart=R;
        this.ImagePart=I;
    }

    public void setRealPart(double realPart) {
        RealPart = realPart;
    }

    public void setImagePart(double imagePart) {
        ImagePart = imagePart;
    }

    public double getRealPart() {
        return RealPart;
    }

    public double getImagePart() {
        return ImagePart;
    }

    public complex complexAdd(complex a) {
        return new complex(this.getRealPart()+a.getRealPart(),this.getImagePart()+a.getImagePart());
    }

    public complex complexSub(complex a) {
        return new complex(this.getRealPart()-a.getRealPart(),this.getImagePart()-a.getImagePart());
    }

    public complex complexMulti(complex a) {
        return new complex(this.getRealPart()*a.getRealPart()-this.getImagePart()*a.getImagePart(),this.getRealPart()*a.getImagePart()+this.getImagePart()*a.getRealPart());
    }

    public complex complexDiv(complex a) {
        double d=Math.sqrt(a.getRealPart()*a.getRealPart())+Math.sqrt(a.getImagePart()*a.getImagePart());
        double e=this.getImagePart()*a.getRealPart()-this.getRealPart()*a.getImagePart();
        double f=this.getRealPart()*a.getRealPart()+this.getImagePart()*a.getImagePart();
        return new complex(f/d, e/d);
    }

    public boolean equals(complex a){
        if(this.RealPart*this.RealPart+this.ImagePart*this.ImagePart > a.RealPart*a.RealPart+a.ImagePart*a.ImagePart)
            return true;
        else
            return false;
    }

    public String toString() {
        if(this.getImagePart()<0)
            return RealPart + "" + ImagePart + "i";
        else if(this.getImagePart()==0)
            return "" + RealPart;
        else
            return RealPart + "+" +ImagePart + "i";
    }
}