package jdk;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.lang.*;


/**
 * Created by besti on 2019/9/29.
 */
public class YuanCS {
    public static void main(String[] args) throws IOException {
        //1.建立一个服务器Socket(ServerSocket)绑定指定端口
        ServerSocket serverSocket=new ServerSocket(8800);
        //2.使用accept()方法阻止等待监听，获得新连接
        Socket socket=serverSocket.accept();
        //3.获得输入流
        InputStream inputStream=socket.getInputStream();
        BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(inputStream));
        //获得输出流
        OutputStream outputStream=socket.getOutputStream();
        PrintWriter printWriter=new PrintWriter(outputStream);
        //4.读取用户输入信息

        System.out.println("服务器已经成功运行......");

        String n=bufferedReader.readLine();
        if(n !=null){
            System.out.println("收到来自客户端的指令：" + n);
        }

        char[] arr=n.toCharArray();
        for(int i=0;i<arr.length;i++)
        {
            System.out.println("arr[i]"+ arr[i]);
        }
        int k=(int)arr[0]-48;
        int j=(int)arr[2]-48;
        int x=(int)arr[5]-48;
        int y=(int)arr[7]-48;
        System.out.println(k);
        System.out.println(j);
        System.out.println(x);
        System.out.println(y);

        String fu;
        fu = String.valueOf(arr[4]);
        ComplexNumber r1;
        r1 = new ComplexNumber(k,j);
        ComplexNumber r2;
        r2 = new ComplexNumber(x,y);
        ComplexNumber r4;
        ComplexNumber r5;
        ComplexNumber r6;
        ComplexNumber r7 ;
        //给客户一个响应
        switch(fu) {
            case "+":
                r4 = r1.ComplexAdd(r2);
                System.out.println(r4);
                String reply1 = "r1 +r2:" + r4;
                printWriter.write(reply1);
                printWriter.flush();
                break;
            case "-":
                r5 = r1.ComplexSub(r2);
                String reply2 = "r1 -r2 :" + r5;
                printWriter.write(reply2);
                printWriter.flush();
                break;
            case "*":
                r6 = r1.ComplexMulti(r2);
                String reply3 = "r1 *r2 :" + r6;
                printWriter.write(reply3);
                printWriter.flush();
                break;
            case "/":
                r7 = r1.ComplexDiv(r2);
                String reply4 = "r1 /r2 :" + r7;
                printWriter.write(reply4);
                printWriter.flush();
                break;
        }
        //5.关闭资源
        printWriter.close();
        outputStream.close();
        bufferedReader.close();
        inputStream.close();
        socket.close();
        serverSocket.close();
    }
}