package jdk;
import java.io.*;
public class FileText4 {
    public static void main(String[] args) throws IOException {
        File file = new File("C:\\Users\\12441\\Desktop", "HelloWorld4.txt");
        if (!file.exists()) {
            file.createNewFile();
        }
        OutputStream outputStream2 = new FileOutputStream(file);
        BufferedOutputStream bufferedOutputStream2 = new BufferedOutputStream(outputStream2);
        String content2 = "利用BufferedOutputStream写入文件的缓冲区内容";
        bufferedOutputStream2.write(content2.getBytes(), 0, content2.getBytes().length);
        bufferedOutputStream2.flush();
        bufferedOutputStream2.close();
        Writer writer2 = new FileWriter(file);
        writer2.write("Hello, I/O Operataion!这是利用Writer写入文件的内容 \n");
        writer2.flush();
        writer2.append("Hello, ultraman,World");
        writer2.flush();
        String content = "";
        Reader reader2 = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(reader2);
        System.out.println("\n下面是用BufferedReader读出的数据：");
        while ((content = bufferedReader.readLine()) != null) {
            System.out.println(content);
        }
    }
}