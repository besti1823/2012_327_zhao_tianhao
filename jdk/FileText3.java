package jdk;

import java.io.*;

public class FileText3 {
    public static void main(String[] args) throws IOException {
        File file = new File("C:\\Users\\12441\\Desktop", "HelloWorld3.txt");
        if (!file.exists()) {
            file.createNewFile();
        }
        OutputStream outputStream2 = new FileOutputStream(file);
        BufferedOutputStream bufferedOutputStream2 = new BufferedOutputStream(outputStream2);
        String content2 = "利用BufferedOutputStream写入文件的缓冲区内容";
        bufferedOutputStream2.write(content2.getBytes(), 0, content2.getBytes().length);
        bufferedOutputStream2.flush();
        bufferedOutputStream2.close();
        Writer writer2 = new FileWriter(file);
        writer2.write("Hello, I/O Operataion!这是利用Writer写入文件的内容\n");
        writer2.flush();
        writer2.append("Hello,MIKU,World");
        writer2.flush();
//        BufferedWriter bufferedWriter = new BufferedWriter(writer2);
//        String content3 = "使用bufferedWriter写入";
//        bufferedWriter.write(content3,0,content3.length());
//        bufferedWriter.flush();
//        bufferedWriter.close();
        Reader reader2 = new FileReader(file);
        System.out.println("下面是用Reader读出的数据：");
        while (reader2.ready()) {
            System.out.print((char) reader2.read() + "  ");
        }
    }
}
