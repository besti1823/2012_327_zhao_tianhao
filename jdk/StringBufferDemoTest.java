package jdk;

import junit.framework.TestCase;
import org.junit.Test;

public class StringBufferDemoTest extends TestCase {
    StringBuffer a = new StringBuffer("Kurumi");
    StringBuffer b = new StringBuffer("Kurumi Yatogami");
    StringBuffer c = new StringBuffer("Kurumi Yatogami Siki");

    @Test
    public void testcharAt() throws Exception {
        assertEquals('K', a.charAt(0));
        assertEquals(' ', b.charAt(6));
        assertEquals('S', c.charAt(16));
    }

    @Test
    public void testcapacity() throws Exception {
        assertEquals(22, a.capacity());
        assertEquals(31, b.capacity());
        assertEquals(36, c.capacity());
    }

    @Test
    public void testlength() throws Exception {
        assertEquals(6, a.length());
        assertEquals(15, b.length());
        assertEquals(20, c.length());
    }

    @Test
    public void testindexOf() throws Exception {
        assertEquals(0, a.indexOf("Kurumi"));
        assertEquals(5, b.indexOf("i "));
        assertEquals(-1, c.indexOf("Tobichi"));
    }
}
