package jdk;

public class EmptyCollectionException3 extends RuntimeException {
    public EmptyCollectionException3(String collection){
        super("The " + collection +" is empty.");
    }
}