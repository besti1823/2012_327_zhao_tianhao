package jdk;

public class ElementNotFoundException extends RuntimeException {
    public ElementNotFoundException (String collection) {
        super (collection + "没有找到特定元素。");
    }
}