package jdk;

public class RealNumber {
    private int a;
    private int b;

    public RealNumber(int a, int b) {
        this.a = a;
        this.b = b;
        if(0==b)this.b=1;
        this.huajian();
    }

    public int getA() {
        return a;
    }

    public int getB() {
        return b;
    }

    public void setA(int a) {
        this.a = a;
    }

    public void setB(int b) {
        this.b = b;
    }

    public RealNumber Add(RealNumber rn){
        int i,j;
        i=this.a*rn.b+this.b*rn.a;
        j=this.b*rn.b;
        return new RealNumber(i,j);
    }

    public RealNumber Sub(RealNumber rn){
        int i,j;
        i=this.a*rn.b-this.b*rn.a;
        j=this.b*rn.b;
        return new RealNumber(i,j);
    }

    public RealNumber Multi(RealNumber rn){
        return new RealNumber(this.a*rn.a,this.b*rn.b);
    }

    public RealNumber Div(RealNumber rn){
        return new RealNumber(this.a*rn.b,this.b*rn.a);
    }

    private void huajian(){
        int i,p=1;
        if(a<0){
            p=-p;
            a=-a;
        }
        if(b<0){
            p=-p;
            b=-b;
        }
        for(i=a;i>1;i--){
            if(a%i==0&&b%i==0)
                break;
        }
        a=a/i;
        b=b/i;
        if(p==-1)a=-a;
    }

    @Override
    public String toString() {
        return a+"/"+b;
    }
}