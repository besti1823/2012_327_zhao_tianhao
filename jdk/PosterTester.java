package jdk;
import java.util.Scanner;
public class PosterTester {
    public static void main(String[] args){
        String expression, again;
        int result;
        Scanner in = new Scanner(System.in);
        do {

                PostEvaluator evaluator = new PostEvaluator();
                System.out.println("输入一个后缀表达式：(eg: 5 4 + 3 2 1 - + *)");
                System.out.println("输入必须是整数，运算符(+,-,*,/)");
                expression = in.nextLine();
                result = evaluator.evaluate(expression);
                System.out.println();
                System.out.println("结果是" +result);
                System.out.println("继续计算[Y/N]?");
                again = in.nextLine();
                System.out.println();
            }
            while (again.equalsIgnoreCase("y"));

    }

}
