package jdk;
//字节流
import java.io.*;
public class FileText1 {
    public static void main(String[] args) throws IOException {
        //（1）文件创建（文件类实例化）
        File file = new File("C:\\Users\\12441\\Desktop","HelloWorld1.txt");
        if (!file.exists()){
            file.createNewFile();
        }
        // file.delete();
        //（2）文件读写
        //第一种：字节流读写，先写后读
        //output写入
        OutputStream outputStream1 = new FileOutputStream(file);
        byte[] hello = {'N','M','S','L','M','N','D','!'};
        outputStream1.write(hello);
        outputStream1.flush();//可有可无，不执行任何操作！！！
        //input读入
        InputStream inputStream1 = new FileInputStream(file);
        while (inputStream1.available()> 0)
        {
            System.out.print((char) inputStream1.read()+"  ");
        }
        inputStream1.close();
        System.out.println("\n文件读写结束：OutputStream、InputStream先写后读,内容彩蛋自己想。");

    }


}