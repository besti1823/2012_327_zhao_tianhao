package jdk;

public class BinaryTreeNode<T>
{
    protected T element;
    protected BinaryTreeNode<T> left, right;

    // 使用指定的数据创建一个新的树结点。
    public BinaryTreeNode(T obj)
    {
        element = obj;
        left = null;
        right = null;
    }

    // 使用指定的数据创建一个新的树结点。
    public BinaryTreeNode(T obj, LinkedBinaryTree<T> left, LinkedBinaryTree<T> right)
    {
        element = obj;
        if (left == null)
            this.left = null;
        else
            this.left = left.getRootNode();

        if (right == null)
            this.right = null;
        else
            this.right = right.getRootNode();
    }

    // 返回该结点的非空子结点数。
    public int numChildren()
    {
        int children = 0;

        if (left != null)
            children = 1 + left.numChildren();

        if (right != null)
            children = children + 1 + right.numChildren();

        return children;
    }

    // 返回结点处的元素。
    public T getElement()
    {
        return element;
    }

    // 返回结点的右孩子
    public BinaryTreeNode<T> getRight()
    {
        return right;
    }

    // 更改结点的右孩子
    public void setRight(BinaryTreeNode<T> node)
    {
        right = node;
    }

    // 返回结点的左孩子
    public BinaryTreeNode<T> getLeft()
    {
        return left;
    }

    // 更改结点的左孩子
    public void setLeft(BinaryTreeNode<T> node)
    {
        left = node;
    }

    // 判定结点是一片叶子或是一个内部结点
    public boolean isLeaf(BinaryTreeNode<T> node)
    {
        return node.numChildren() == 0;
    }}
