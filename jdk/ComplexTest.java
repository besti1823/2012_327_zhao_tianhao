package jdk;

import junit.framework.TestCase;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
//复数运算test，加减乘除，判断真值实虚
public class ComplexTest extends TestCase {
    Complex a = new Complex(1, 3);
    Complex b = new Complex(1, -5);
    Complex c = new Complex(20, 0);
    Complex d = new Complex(0, -4);
    Complex e = new Complex(7, 0);

    @Before
    public void before() throws Exception {
    }

    @After
    public void after() throws Exception {
    }

    @Test
    public void testGetRealPart() throws Exception {
//TODO: Test goes here...
        assertEquals(1.0, a.getRealPart());
    }

    @Test
    public void testGetImagePart() throws Exception {
//TODO: Test goes here...
        assertEquals(3.0, a.getImagePart());
    }
    @Test
    public void testEquals() throws Exception {
//TODO: Test goes here...
        assertEquals(true, c.equals(20, 0));
        assertEquals(false, d.equals(0, 0));
    }
    @Test
    public void testComplexAdd() throws Exception {
//TODO: Test goes here...   //测试加法
        assertEquals("2.0-2.0i", a.ComplexAdd(b));
    }
    @Test
    public void testComplexSub() throws Exception {
//TODO: Test goes here...  //测试减法
        assertEquals("0.0+8.0i", a.ComplexSub(b));
    }

    @Test
    public void testComplexMulti() throws Exception {
//TODO: Test goes here...   //测试乘法
        assertEquals("16.0-2.0i", a.ComplexMulti(b));
    }
    @Test
    public void testComplexDiv() throws Exception {
//TODO: Test goes here...
        assertEquals("-0.5384615384615384+0.011834319526627227i", a.ComplexDiv(b));
    }

}