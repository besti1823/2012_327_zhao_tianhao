package jdk;

public class box {
    private double length;
    private double width;
    private double depth;

    public box(double length,double width,double depth){
        this.length=length;
        this.width=width;
        this.depth=depth;
    }
    public void setLength(double a){
        length=a;
    }
    public double getLength(){
        return length;
    }
    public void setWidth(double a){
        width=a;
    }
    public  double getWidth(){
        return width;
    }
    public void setDepth(double a){
        depth=a;
    }
    public double getDepth() {
        return depth;
    }
    public String toString() {
        boolean full = false;
        return "empty"+ "长度为"+length+";宽度为"+width+";深度为"+depth;
    }
}