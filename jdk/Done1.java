package jdk;

import java.util.Scanner;

public class Done1 {
    public static void main(String[] args) {
        Scanner in=new Scanner(System.in);
        String a=null;
        do{
            try{
                System.out.println("请输入一串字符 (以DONE为输入末尾):");
                a=in.nextLine();
                if(a.length()>=10){
                    throw new StringToolongException("输入过多字符！");
                }
            }
            catch (StringToolongException e){
                System.out.println("输入过多异常!");
            }
        }while(!a.equals("DONE"));
    }
}