package jdk;

import java.util.Iterator;

public interface ListADT<T>  extends Iterable<T>{
    // 删除并且返回列表的第一个元素
    public T removeFirst();

    // 删除并且返回列表的最后一个元素
    public T removeLast();

    // 删除并且返回列表的特定元素
    public T remove(T element);

    // 返回列表的第一个元素
    public T first();

    // 返回列表的最后一个元素
    public T last();

    // 如果列表包含目标元素就返回true
    public boolean contains(T target);

    // 如果列表空则返回true
    public boolean isEmpty();

    // 返回列表个数
    public int size();

    // 返回列表元素的迭代器
    public Iterator<T> iterator();

    // 返回列表的字符串形式
    public String toString();
}