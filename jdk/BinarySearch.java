package jdk;

import java.util.Arrays;

public class BinarySearch {

    public static
    boolean binarySearch(int[] data, int min, int max, int target)
    {
        boolean found = false;
        int midpoint = (min + max) / 2;  // determine the midpoint

        if (data[midpoint]==target)
            found = true;

        else if (data[midpoint]>target)
        {
            if (min <= midpoint - 1)
                found = binarySearch(data, min, midpoint - 1, target);
        }

        else if (midpoint + 1 <= max)
            found = binarySearch(data, midpoint + 1, max, target);

        return found;
    }

}