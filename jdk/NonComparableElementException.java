package jdk;

public class NonComparableElementException extends RuntimeException{
    public NonComparableElementException(String collection) {
        super(collection + "没有可比较的元素。");
    }
}