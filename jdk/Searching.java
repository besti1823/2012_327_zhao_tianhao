package jdk;

import java.util.Arrays;





public class Searching <T extends Comparable<? super T>> {

//顺序查找（线性查找）
    public static <T extends Comparable<? super T>> boolean linearSearch(T[] data, int min, int max, T target) {


        int index = min;


        boolean found = false;





        while (!found && index <= max) {


            found = data[index].equals(target);


            index++;


        }


        return found;


    }




//折半查找（二分法查找）
    public static <T extends Comparable<? super T>> boolean binarySearch(T[] data, int min, int max, T target) {


        boolean found = false;


        int midpoint = (min + max) / 2;





        if (data[midpoint].compareTo(target) == 0)


            found = true;





        else if (data[midpoint].compareTo(target) > 0) {


            if (min <= midpoint - 1)


                found = binarySearch(data, min, midpoint - 1, target);


        } else if (midpoint + 1 <= max)


            found = binarySearch(data, midpoint + 1, max, target);





        return found;


    }

// 二叉查找树


    public boolean BinarySearchTreeSearch(T[] data, int low, int high, T target)


    {


        boolean found = false;


        LinkedBinarySearchTree linkedBinarySearchTree = new LinkedBinarySearchTree();


        // Comparable<T> comparableElement = (Comparable<T>)target;


        for (int i= low;i<= high;i++)


            linkedBinarySearchTree.addElement(data[i]);





        while (found == false&&linkedBinarySearchTree.root!=null) {


            if (target.equals(linkedBinarySearchTree.root.getElement())){


                found = true;


            }


            else


            {


                if (target.compareTo((T)linkedBinarySearchTree.root.getElement()) < 0)


                    linkedBinarySearchTree.root = linkedBinarySearchTree.root.left;


                else


                    linkedBinarySearchTree.root = linkedBinarySearchTree.root.getRight();


            }


        }


        return found;


    }



    // 哈希查找（散列查找）


    public static <T extends Comparable<? super T>> boolean hashSearch(int[] data, int target)


    {


        boolean found = false;


        HashSearching hashSearching = new HashSearching(data.length);


        hashSearching.addInAddress(data);






        if (hashSearching.search(data,target))


        {


            found = true;


        }





        return found;


    }












}