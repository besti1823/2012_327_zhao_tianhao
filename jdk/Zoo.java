package jdk;

public class Zoo {
    private Animal[] animallist;
    public Zoo() {
        animallist = new Animal[5];
        animallist[0] = new Cat("我们一起学猫叫一起喵喵喵喵喵！","1001");
        animallist[1] = new Dog("我们一起学狗叫一起哼哼哼哼哼！","1002");
        animallist[2] = new Son("我们一起学儿子叫一起爸爸爸爸爸！","1003");
    }
    public void feedall(){
        for (Animal animal:animallist){
            System.out.println(animal);
            animal.feed();
        }
       
    }
}
