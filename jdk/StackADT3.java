package jdk;

public interface StackADT3<T> {


    public void NumberInsert(int where, int num);
    public void WhereDelete(int where);
    public boolean isEmpty();
    public int size();

    public T pop();
    public void push(T element);

    @Override
    public String toString();
    public String MaoPaoSorting(String s);

}