package jdk;

import static java.lang.Math.random;

public class pp53 {
    public enum value {heads, name, tails};
    private  int b;
    private value a= value.heads,c= value.tails;
    public pp53 (){
        flip();
    }
    public void flip(){
        b=(int)(random()*2);
    }
    public String isHeads(){
        if(b==0){
            return a.name();
        }
        else{
            return c.name();
        }
    }
    public String toString(){
        return (b==0)?"heads":"tails";
    }
}
