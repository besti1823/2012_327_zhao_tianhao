package jdk;

import jdk.MyUtil;
import org.junit.Assert;
import org.junit.Test;
import junit.framework.TestCase;
public class MyUtilTest {
    @Test
    public void testNormal() {
        Assert.assertEquals("不及格", MyUtil.percentage2fivegrade(55));
        Assert.assertEquals("及格", MyUtil.percentage2fivegrade(65));
        Assert.assertEquals("中等", MyUtil.percentage2fivegrade(75));
        Assert.assertEquals("良好", MyUtil.percentage2fivegrade(85));
        Assert.assertEquals("优秀", MyUtil.percentage2fivegrade(95));
    }
    @Test
    public void testExceptions() {
        Assert.assertEquals("错误!", MyUtil.percentage2fivegrade(105));
        Assert.assertEquals("错误", MyUtil.percentage2fivegrade(-55));
    }

    @Test
    public void testBoundary() {
        Assert.assertEquals("不及格", MyUtil.percentage2fivegrade(0));
        Assert.assertEquals("及格", MyUtil.percentage2fivegrade(60));
        Assert.assertEquals("中等", MyUtil.percentage2fivegrade(70));
        Assert.assertEquals("良好", MyUtil.percentage2fivegrade(80));
        Assert.assertEquals("优秀", MyUtil.percentage2fivegrade(90));
        Assert.assertEquals("优秀", MyUtil.percentage2fivegrade(100));
    }
}