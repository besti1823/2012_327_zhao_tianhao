package jdk;

public class Son extends Animal {
    public Son(String name, String id) {
        super(name, id);
    }

    @Override
    public void feed() {
        System.out.println("我们一起学儿子叫一起爸爸爸爸爸！");
    }
}
