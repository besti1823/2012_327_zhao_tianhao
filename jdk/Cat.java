package jdk;

public class Cat extends Animal{

    public Cat(String name, String id) {
        super(name, id);
    }

    @Override
    public void feed() {
        System.out.println("我们一起学猫叫一起喵喵喵喵喵！");
    }
}
