package jdk;

public interface UnorderedListADT<T> extends ListADT<T> {
    // 列表头添加
    public void addToFront(T element);

    // 列表尾添加
    public void addToRear(T element);

    // 目标元素后添加
    public void addAfter(T element, T target);

}