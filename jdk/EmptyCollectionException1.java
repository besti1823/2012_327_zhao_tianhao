package jdk;

public class EmptyCollectionException1 extends RuntimeException {
    public EmptyCollectionException1(String collection){
        super("The " + collection +" is empty.");
    }
}