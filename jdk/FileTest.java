package jdk;
import java.io.*;
import java.util.Scanner;
public class FileTest
{
    public static void main(String[] args) throws IOException {
        File file = new File("C:\\Users\\12441\\Desktop", "ComplexAdd.txt");
        if (!file.exists())
        {
            file.createNewFile();
        }
        // 写入文件，输入复数
        String temp = null;
        Writer writer2 = new FileWriter(file);
        System.out.println("请输入第一个复数的实部：");
        Scanner scan = new Scanner(System.in);
        temp = scan.nextLine();
        writer2.write(temp +"\r\n");
        writer2.flush();
        System.out.println("请输入第一个复数的虚部(纯数字即可，自动补i)：");
        temp = scan.nextLine();
        writer2.write(temp +"\r\n");
        writer2.flush();
        System.out.println("请输入第二个复数的实部：");
        temp = scan.nextLine();
        writer2.append(temp+"\r\n");
        writer2.flush();
        System.out.println("请输入第二个复数的虚部(纯数字即可，自动补i)：");
        temp = scan.nextLine();
        writer2.append(temp+"\r\n");
        writer2.flush();
        //从文件里读出复数，转换成数字  计算
        Reader reader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(reader);
        Float[] num = new Float[4];
        float RealPart,ImagePart;
      //判断步骤并输出结果
        for (int i = 0;i<4;i++)
        {
            num[i] = Float.parseFloat(bufferedReader.readLine());
            // System.out.println(num[i]);
        }
        if (num[1]>0)
            System.out.print( "" + num[0] + "+" + num[1] + "i+");
        else
            System.out.print("" +num [0] + num[1] + "i+");
        if(num[3]>0)
            System.out.print("" +num [2] + "+" + num[3] + "i =");
        else
            System.out.print("" + num[2] + num[3] + "i =");
        RealPart = num[0]+num[2];
        ImagePart = num[1]+num[3];
        if (ImagePart>0)
            System.out.println(RealPart + "+" + ImagePart + "i");
        else
            System.out.println( "" + RealPart + ImagePart + "i");
    }
}