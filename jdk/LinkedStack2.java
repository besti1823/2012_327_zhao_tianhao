package jdk;
import jdk.EmptyCollectionException2;
public class LinkedStack2<T> implements StackADT2<T>
{

    private final int DEFAULT=100;

    private LinnearNode2 Head;
    private int count;
    private String[] Array,temp,stack;


    @Override
    public String toString() {
        String result = "";
        System.out.println("栈中的元素是：" + "\t");

        for(LinnearNode2 current = this.Head; current != null; current = current.getNext()) {
            result = result + current.getElement().toString() + "\t";
        }
        return result;
    }

    @Override
    public String MaoPaoSorting() {
        return null;
    }

    public T pop() throws jdk.EmptyCollectionException2 {
        if (this.isEmpty()) {
            throw new EmptyCollectionException("Stack");
        } else {
            T result = (T) this.Head.getElement();
            this.Head = this.Head.getNext();
            this.count--;
            return result;
        }
    }

    @Override
    public void push(T element) {
        LinnearNode2 temp = new LinnearNode2(element);
        temp.setNext(this.Head);
        this.Head = temp;
        this.count++;
    }


    @Override
    public void NumberInsert(int where, int num) {
        LinnearNode2 node1,node2;
        LinnearNode2 temp = new LinnearNode2(num);

        if (where==0){
            temp.next = Head;
            Head =temp;
        }
        else {
            node1=Head;
            node2=Head.next;

            for (int i =0; i<where-1;i++){
                node1=node1.next;
                node2=node2.next;
            }
            if (node2!=null){
                temp.next=node2;
                node1.next=temp;
            }
            else {
                node1.next=temp;
            }
            count++;
        }
    }


    @Override
    public void WhereDelete(int where) {
        LinnearNode2 node1=Head,node2=null;
        int i =0;
        if (node1==null)
            System.out.println("链表中无元素！");
        else
            node2 = node1.next;
        while(node1.next!=null&&node2.next!=null&&i<where-1){
            node1=node1.next;
            node2=node2.next;
            i++;
        }

        node1.next = node2.next;
        count--;
    }


    @Override
    public boolean isEmpty() {
        return this.count == 0;
    }

    @Override
    public int size() {
        return count;
    }



}