package jdk;

public class LinnearNode3<T> {
    public LinnearNode3 next;
    private T element;

    public LinnearNode3(){
        next=null;
        element=null;
    }
    public LinnearNode3(T elem)
    {
        next = null;
        element = elem;
    }

    public LinnearNode3<T> getNext(){
        return  next;
    }
    public void setNext(LinnearNode3 node){
        next = node;
    }
    public T getElement(){
        return element;
    }

    public void setElement(T elem) {
        element=elem;
    }
}