package jdk;

public class LinkedStack1<T> implements StackADT1<T>
{
    private final int DEFAULT=100;

    private LinnearNode1 top;
    private int count;
    private  T[] stack;
    @Override
    public T pop() throws EmptyCollectionException1 {
        if (this.isEmpty()) {
            throw new EmptyCollectionException1("Stack");
        } else {
            T result = (T) this.top.getElement();
            this.top = this.top.getNext();
            this.count--;
            return result;
        }
    }

    @Override
    public void push(T element) {
        LinnearNode1<T> temp = new LinnearNode1(element);
        temp.setNext(this.top);
        this.top = temp;
        this.count++;
    }
    @Override
    public T peek() throws EmptyCollectionException1 {
        if (this.isEmpty()) {
            throw new EmptyCollectionException1("Stack");
        } else {
            return (T) this.top.getElement();
        }
    }
    @Override
    public boolean isEmpty() {
        return this.count == 0;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public String toString() {
        String result = "";
        System.out.println("ջ�е�Ԫ���ǣ�" + "\t");

        for(LinnearNode1 current = this.top; current != null; current = current.getNext()) {
            result = result + current.getElement().toString() + "\t";
        }
        return result;
    }
}