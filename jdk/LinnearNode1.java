package jdk;

public class LinnearNode1<T> {
    public LinnearNode1 next;
    private T element;

    public LinnearNode1(){
        next=null;
        element=null;
    }
    public LinnearNode1(T elem)
    {
        next = null;
        element = elem;
    }

    public LinnearNode1<T> getNext(){
        return  next;
    }
    public void setNext(LinnearNode1 node){
        next = node;
    }
    public T getElement(){
        return element;
    }

    public void setElement(T elem) {
        element=elem;
    }
}