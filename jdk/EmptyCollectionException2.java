package jdk;

public class EmptyCollectionException2 extends RuntimeException {
    public EmptyCollectionException2(String collection){
        super("The " + collection +" is empty.");
    }
}