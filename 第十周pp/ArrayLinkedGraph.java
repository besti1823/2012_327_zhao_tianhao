package com.company;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;



//图表示图形的邻接链表实现。
public class ArrayLinkedGraph<T> implements GraphADT<T> {

    protected int numVertices;// 图中的顶点数
    protected ArrayList<LinearNode> arrayList = new ArrayList<LinearNode>();// 邻接链表
    protected T[] vertices;// 顶点的值
    protected int modCount;

    //创建一个空图形。
    public ArrayLinkedGraph(){
        numVertices = 0;
        modCount++;
    }

    //返回邻接链表的字符串表示形式。
    public void printNodeList() {
        for (int i = 0; i < arrayList.size(); i++) {
            LinearNode nodes = arrayList.get(i);
            System.out.print("顶点：" + arrayList.get(i).getElement());
            while (nodes.getNext() != null) {
                System.out.print(" --> " + nodes.getNext().getElement());
                nodes = nodes.getNext();
            }
            System.out.println(";");
        }
    }

    //将顶点添加到图形中, 扩展图形的容量如有需要。 它还将对象与顶点相关联。
    @Override
    public void addVertex(T vertex) {
        LinearNode LinearNode = new LinearNode(vertex);
        arrayList.add(LinearNode);
        numVertices++;
        modCount++;
    }

    @Override
    public void removeVertex(T vertex) {
        LinearNode LinearNode = new LinearNode(vertex);
        arrayList.remove(LinearNode);
        numVertices--;
        modCount++;
    }

    /**
     * 返回顶点第一次出现的索引值。
     * 如果找不到密钥, 则返回-1。
     *
     * @param vertex 正在寻找其索引值的顶点
     * @return 给定顶点的索引值
     */
    public int getIndex(T vertex){
        LinearNode LinearNode = new LinearNode(vertex);
        for(int i = 0;i<numVertices;i++){
            if(arrayList.get(i).element==vertex)
                return i;
        }
        return -1;
    }

    //添加一条边
    @Override
    public void addEdge(T vertex1, T vertex2) {
        if(getIndex(vertex1)!=-1&&getIndex(vertex2)!=-1){
            LinearNode<T> LinearNode1 = arrayList.get(getIndex(vertex1));
            LinearNode<T> LinearNode2 = arrayList.get(getIndex(vertex2));
            LinearNode<T> temp1 = new LinearNode<>(vertex1);
            LinearNode<T> temp2 = new LinearNode<>(vertex2);
            while(LinearNode1.getNext() !=null)
                LinearNode1 = LinearNode1.getNext();
            LinearNode1.setNext(temp2);
            while(LinearNode2.getNext() !=null)
                LinearNode2 = LinearNode2.getNext();
            LinearNode2.setNext(temp1);
        }

    }

    //删除一条边
    @Override
    public void removeEdge(T vertex1, T vertex2) {

        if(getIndex(vertex1)!=-1&&getIndex(vertex2)!=-1){
            LinearNode<T> LinearNode1 = arrayList.get(getIndex(vertex1));
            LinearNode<T> LinearNode2 = arrayList.get(getIndex(vertex2));
            LinearNode<T> temp1 = null,temp2 = null;

            while(LinearNode1.getElement() != vertex2 ) {
                temp1 = LinearNode1;
                LinearNode1 = LinearNode1.getNext();
            }
            if(LinearNode1.getNext()==null)
                temp1.setNext(null);
            else{
                LinearNode<T> temp = LinearNode1.getNext();
                temp1.setNext(temp);
            }


            while(LinearNode2.getElement() != vertex1  ) {
                temp2 = LinearNode2;
                LinearNode2 = LinearNode2.getNext();
            }
            if(LinearNode2.getNext()==null)
                temp2.setNext(null);
            else{
                LinearNode<T> temp = LinearNode2.getNext();
                temp2.setNext(temp);
            }
        }
    }

    //返回从给定索引开始执行深度优先遍历的迭代器。
    @Override
    public Iterator iteratorDFS(T startVertex) {
        return iteratorDFS(getIndex(startVertex));

    }

    public Iterator<T> iteratorDFS(int startIndex)
    {
        Integer x;
        boolean found;
        this.vertices = (T[])(new Object[numVertices]);
        StackADT<Integer> traversalStack = new LinkedStack<Integer>();
        UnorderedListADT<T> resultList = (UnorderedListADT<T>) new ArrayUnorderedList<T>();
        boolean[] visited = new boolean[numVertices];

        if (!indexIsValid(startIndex))
            return resultList.iterator();

        for (int i = 0; i < numVertices; i++)
            visited[i] = false;

        traversalStack.push(startIndex);
        resultList.addToRear((T)arrayList.get(startIndex).getElement());
        visited[startIndex] = true;

        while (!traversalStack.isEmpty())
        {
            x = traversalStack.peek();
            found = false;

            //Find a vertex adjacent to x that has not been visited
            //     and push it on the stack
            for (int i = 0; (i < numVertices) && !found; i++)
            {
                if (isEdge(x,i) && !visited[i])
                {
                    traversalStack.push(i);
                    resultList.addToRear((T)arrayList.get(i).getElement());
                    visited[i] = true;
                    found = true;
                }
            }
            if (!found && !traversalStack.isEmpty())
                traversalStack.pop();
        }
        return new GraphIterator(resultList.iterator());
    }

    //返回从给定索引开始执行广度优先遍历的迭代器。
    @Override
    public Iterator iteratorBFS(T startVertex) {
        return iteratorBFS(getIndex(startVertex));

    }
    public Iterator<T> iteratorBFS(int startIndex)
    {
        Integer x;
        QueueADT<Integer> traversalQueue = new LinkedQueue<Integer>();
        UnorderedListADT<T> resultList = (UnorderedListADT<T>) new ArrayUnorderedList<T>();

        if (!indexIsValid(startIndex))
            return resultList.iterator();

        boolean[] visited = new boolean[numVertices];
        for (int i = 0; i < numVertices; i++)
            visited[i] = false;

        traversalQueue.enqueue(startIndex);
        visited[startIndex] = true;

        while (!traversalQueue.isEmpty())
        {
            x = traversalQueue.dequeue();
            resultList.addToRear((T)arrayList.get(x).getElement());

            //Find arraylistl vertices adjacent to x that have not been visited
            //     and queue them up

            for (int i = 0; i < numVertices; i++)
            {
                if (isEdge(x,i) && !visited[i])
                {
                    traversalQueue.enqueue(i);
                    visited[i] = true;
                }

            }
        }
        return new GraphIterator(resultList.iterator());
    }



    protected boolean indexIsValid(int index)
    {
        if (index<numVertices)
            return true;
        else
            return false;
    }

    public boolean isEdge(int i, int j){
        if(i==j)
            return false;
        LinearNode LinearNode1 = arrayList.get(i);
        LinearNode LinearNode2 = arrayList.get(j);
        while(LinearNode1!=null) {
            if (LinearNode1.getElement() == LinearNode2.getElement())
                return true;
            LinearNode1 =LinearNode1.getNext();
        }
        return false;
    }

    //返回一个迭代器, 其中包含位于两个给定顶点之间最短路径中的顶点的索引。
    protected Iterator<Integer> iteratorShortestPathIndices
    (int startIndex, int targeLtIndex)
    {
        int index = startIndex;
        int[] pathLength = new int[numVertices];
        int[] predecessor = new int[numVertices];
        QueueADT<Integer> traversalQueue = new LinkedQueue<Integer>();
        UnorderedListADT<Integer> resultList =
                (UnorderedListADT<Integer>) new ArrayUnorderedList<Integer>();

        if (!indexIsValid(startIndex) || !indexIsValid(targeLtIndex) ||
                (startIndex == targeLtIndex))
            return resultList.iterator();

        boolean[] visited = new boolean[numVertices];
        for (int i = 0; i < numVertices; i++)
            visited[i] = false;

        traversalQueue.enqueue(Integer.valueOf(startIndex));
        visited[startIndex] = true;
        pathLength[startIndex] = 0;
        predecessor[startIndex] = -1;

        while (!traversalQueue.isEmpty() && (index != targeLtIndex))
        {
            index = (traversalQueue.dequeue()).intValue();

            //Update the pathLength for each unvisited vertex adjacent
            //     to the vertex at the current index.
            for (int i = 0; i < numVertices; i++)
            {
                if (isEdge(index,i) && !visited[i])
                {
                    pathLength[i] = pathLength[index] + 1;
                    predecessor[i] = index;
                    traversalQueue.enqueue(Integer.valueOf(i));
                    visited[i] = true;
                }
            }
        }
        if (index != targeLtIndex)  // no path must have been found
            return resultList.iterator();

        StackADT<Integer> stack = new LinkedStack<Integer>();
        index = targeLtIndex;
        stack.push(Integer.valueOf(index));
        do
        {
            index = predecessor[index];
            stack.push(Integer.valueOf(index));
        } while (index != startIndex);

        while (!stack.isEmpty())
            resultList.addToRear(((Integer)stack.pop()));

        return new GraphIndexIterator(resultList.iterator());
    }

    //返回一个迭代器, 其中包含之间的最短路径两个顶点。
    @Override
    public Iterator iteratorShortestPath(T startVertex, T targetVertex) {
        return iteratorShortestPath(getIndex(startVertex), getIndex(targetVertex));
    }

    public Iterator<T> iteratorShortestPath(int startIndex, int targetIndex)
    {
        UnorderedListADT<T> resultList = (UnorderedListADT<T>) new ArrayUnorderedList<T>();
        if (!indexIsValid(startIndex) || !indexIsValid(targetIndex))
            return resultList.iterator();

        Iterator<Integer> it = iteratorShortestPathIndices(startIndex,
                targetIndex);
        while (it.hasNext())
            resultList.addToRear((T)arrayList.get(((Integer)it.next())).getElement());
        return new GraphIterator(resultList.iterator());
    }


    //返回图形中的顶点数。
    @Override
    public int size() {
        return numVertices;
    }
    /**
     * 如果图形为空, 否则返回 true。
     *
     * 如果图形为空, @return 为 true
     */
    @Override
    public boolean isEmpty() {
        if(numVertices ==0)
            return true;
        else
            return false;
    }


    /**
     * 如果图形连接, 则返回 true, 否则返回 false。
     *
     * 如果图形连接, @return 为真
     */
    @Override
    public boolean isConnected() {
        boolean result = true;
        for(int i=0;i<numVertices;i++){
            int temp=0;
            temp=getSizeOfIterator(iteratorBFS(i));
            if(temp!=numVertices)
            {
                result = false;
                break;
            }
        }
        return result;
    }

    private int getSizeOfIterator(Iterator iterator) {
        int size = 0;
        while(iterator.hasNext()){
            size++;
            iterator.next();
        }
        return size;
    }




    //内部类, 用于表示此图元素上的迭代器
    public class GraphIterator implements Iterator<T>
    {
        private int expectedModCount;
        private Iterator<T> iter;


        //使用指定的迭代器设置此迭代器。
        public GraphIterator(Iterator<T> iter)
        {
            this.iter = iter;
            expectedModCount = modCount;
        }



        //如果此迭代器至少有一个其他元素, 则返回 true在迭代中交付。
        public boolean hasNext() throws ConcurrentModificationException
        {
            if (!(modCount == expectedModCount))
                throw new ConcurrentModificationException();

            return (iter.hasNext());
        }


        //返回迭代中的下一个元素。如果没有在此迭代中的更多元素, 一个 nosuchelementexception 是扔了。
        public T next() throws NoSuchElementException
        {
            if (hasNext())
                return (iter.next());
            else
                throw new NoSuchElementException();
        }


        //不支持删除操作。
        public void remove()
        {
            throw new UnsupportedOperationException();
        }
    }


    //内部类, 用于在此图的索引上表示迭代器
    public class GraphIndexIterator implements Iterator<Integer>
    {
        private int expectedModCount;
        private Iterator<Integer> iter;

        //使用指定的迭代器设置此迭代器。
        public GraphIndexIterator(Iterator<Integer> iter)
        {
            this.iter = iter;
            expectedModCount = modCount;
        }

        /**
         * 如果此迭代器至少有一个其他元素, 则返回 true
         * 在迭代中交付。
         *
         * 如果此迭代器至少有一个要传递的元素, @return 为真
         * 在迭代中
         * @throws ConcurrentModificationException (如果集合已更改)
         * 迭代器正在使用时
         */
        public boolean hasNext() throws ConcurrentModificationException
        {
            if (!(modCount == expectedModCount))
                throw new ConcurrentModificationException();

            return (iter.hasNext());
        }

        /**
         * 返回迭代中的下一个元素。如果没有
         * 在此迭代中的更多元素, 一个 nosuchelementexception 是
         * 扔了。
         *
         * @return 迭代中的下一个元素
         * @throws NoSuchElementException, 则为 "nosuchelementexception"
         */
        public Integer next() throws NoSuchElementException
        {
            if (hasNext())
                return (iter.next());
            else
                throw new NoSuchElementException();
        }

        /**
         * 不支持删除操作。
         * @throws UnsupportedOperationException, 如果调用删除操作
         */
        public void remove()
        {
            throw new UnsupportedOperationException();
        }
    }
}
