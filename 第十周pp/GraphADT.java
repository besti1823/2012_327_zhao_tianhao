package com.company;

import java.util.Iterator;
public interface GraphADT<T>
{
    //插入顶点
    public void addVertex(T vertex);
    //删除顶点
    public void removeVertex(T vertex);
    //插入边
    public void addEdge(T vertex1, T vertex2);
    //删除边
    public void removeEdge(T vertex1, T vertex2);
    //返回从给定顶点开始的广度优先遍历。
    public Iterator iteratorBFS(T startVertex);
    //返回从给定顶点开始的深度优先遍历。
    public Iterator iteratorDFS(T startVertex);
    //返回一个迭代器, 其中包含之间的最短路径两个顶点。
    public Iterator iteratorShortestPath(T startVertex, T targetVertex);
    //如果此图为空, 则返回 true, 否则返回 false。
    public boolean isEmpty();
    //如果此图连接, 则返回 true, 否则返回 false。
    public boolean isConnected();
    //返回此图中的顶点数。
    public int size();
    //返回邻接矩阵的字符串表示形式。
    public String toString();
}