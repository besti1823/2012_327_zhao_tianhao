package com.company;

import java.util.Iterator;

public class RunArrayLinkedGraph {
    public static void main(String[] args) {
        ArrayLinkedGraph mg = new ArrayLinkedGraph();
        mg.addVertex(1);
        mg.addVertex(2);
        mg.addVertex(3);
        mg.addVertex(4);
        mg.addVertex(5);
        mg.addEdge(1,2);
        mg.addEdge(1,3);
        mg.addEdge(2,4);
        mg.addEdge(3,5);
        System.out.println("\n检测是否为空：" + mg.isEmpty());
        System.out.println("该图的大小为：" + mg.size());

        System.out.println("初始：");
        mg.printNodeList();

        Iterator x1 = mg.iteratorBFS(3);
        while(x1.hasNext())
            System.out.print(x1.next()+" ");
        System.out.println();
        Iterator x2 = mg.iteratorBFS(3);
        while(x2.hasNext())
            System.out.print(x2.next() + " ");
        System.out.println();

        Iterator x3 = mg.iteratorShortestPath(4,3);
        while(x3.hasNext())
            System.out.print(x3.next()+" ");
        System.out.println();

        System.out.println(mg.isEdge(0,1));
        System.out.println(mg.isEdge(1,2));
    }
}