package week8;

import java.util.Scanner;

public class ContactSearch {
    public static void main(String[] args) {
        Contact[] c = new Contact[6];
        c[0] = new Contact("z", "th", "10000000");
        c[1] = new Contact("s", "mz", "40000000");
        c[2] = new Contact("h", "lh", "50000000");
        c[3] = new Contact("w", "dz", "30000000");
        c[4] = new Contact("x", "yy", "20000000");
        c[5] = new Contact("d", "ls", "60000000");
        sort(c);
        for (int i = 0; i < c.length; i++) {
            System.out.println(c[i]);
        }
        System.out.println("请输入数字:");
        Scanner scan=new Scanner(System.in);
        String m=scan.nextLine();
        String result="false";
//        线性查找
        for(int j=0;j<c.length;j++){
            if(c[j].phone.equals(m)){
                result="true";
            }
        }
        System.out.println(result);
//        二分查找
        String result1="false";
        Contact temp=new Contact("a","b",m);
        int low=0;
        int high=c.length-1;
        int mid=(high+low)/2;
        while(low<=high){
            if(c[mid].compareTo(temp)==0){
                result1="true";
            }
            else if(c[mid].compareTo(temp)<0){
                low=mid+1;
            }
            else{
                high=mid-1;
            }
            mid=(high+low)/2;
        }
        System.out.println(result1);
    }
    public static void sort(Contact[] a){
        Contact[] cc=a;
        Contact temp;
        for(int i=0;i<cc.length;i++){
            int max=i;
            for(int j=i;j<cc.length;j++){
                if(cc[j].compareTo(cc[i])>0){
                    temp=cc[i];
                    cc[i]=cc[j];
                    cc[j]=temp;
                }
            }
        }

    }

}