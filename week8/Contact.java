package week8;

public class Contact implements Comparable {

    protected String Firstname, Secondname, phone;

    public Contact(String F, String S, String N) {
        Firstname = F;
        Secondname = S;
        phone = N;
    }

    public String toString() {
        return Secondname +"  "+Firstname + ": " + phone;
    }

    @Override
    public int compareTo(Object o) {
        int result;
        result=phone.compareTo(((Contact) o).phone);
        return result;
    }
}