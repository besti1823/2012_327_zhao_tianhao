package Tree;

import java.util.LinkedList;
import java.util.Queue;

class Node<T> {
    T val;
    Node left;
    Node right;

    public Node() {
    }

    public Node(T val) {
        this.val = val;
    }

    public Node(T val, Node left, Node right) {
        this.val = val;
        this.left = left;
        this.right = right;
    }
}
//AB#CD###E#F##
public class BinaryTree {
    private static char[] arr = {'A', 'B', '#', 'C', 'D', '#', '#', '#', 'E', '#', 'F', '#', '#'};
    private static int count = 0;

    //构建二叉树(#代表空结点)

    public static Node Create() {
        if (count >= arr.length || arr[count] == '#') {
            count++;// 跳过空结点
return null;        }
        Node node = new Node<>(arr[count++]);
        node.left = Create();
        node.right = Create();
        return node;
    }

    //先序遍历

    public static void preOrder(Node root) {
        if (root != null) {
            System.out.print(root.val + " ");
            preOrder(root.left);
            preOrder(root.right);
        }
    }

    //中序遍历

    public static void inOrder(Node root) {
        if (root != null) {
            inOrder(root.left);
            System.out.print(root.val + " ");
            inOrder(root.right);
        }
    }

    //后序遍历

    public static void postOrder(Node root) {
        if (root != null) {
            postOrder(root.left);
            postOrder(root.right);
            System.out.print(root.val + " ");
        }
    }

    //层次遍历


    public static void levOrder(Node root) {
        if (root != null) {
            Node p = root;
            Queue<Node> queue = new LinkedList<>();
            queue.add(p);
            while (!queue.isEmpty()) {
                p = queue.poll();
                System.out.print(p.val + " ");
                if (p.left != null) {
                    queue.add(p.left);
                }
                if (p.right != null) {
                    queue.add(p.right);
                }
            }
        }
    }

    //计算叶子节点的个数

    public static int getSize(Node root) {
        if (root == null) {
            return 0;
        }
        if (root.left == null && root.right == null) {
            return 1;
        } else {
            return getSize(root.left) + getSize(root.right);
        }
    }

    //计算二叉树的高度

    public static int getHeight(Node root) {
        if (root == null) {
            return 0;
        }
        int leftHeight = getHeight(root.left);
        int rightHeight = getHeight(root.right);
        return (leftHeight > rightHeight) ? (leftHeight + 1) : (rightHeight + 1);
    }

    public static void main(String[] args) {
        System.out.println("\n输入为：AB#CD###E#F##");
        Node root = Create();
        System.out.println("\n先序遍历：");
        preOrder(root);

        System.out.println("\n中序遍历：");
        inOrder(root);

        System.out.println("\n后序遍历：");
        postOrder(root);

        System.out.println("\n层次遍历：");
        levOrder(root);

        System.out.println("\n二叉树叶子结点数：" + getSize(root));
        System.out.println("二叉树高度：" + getHeight(root));
    }
}
