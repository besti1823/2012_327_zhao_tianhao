package week10;

import java.util.Iterator;

public class LinkedBinaryTreeTest {
    public static void main(String[] args) {
        LinkedBinaryTree linkedBinaryTree2 = new LinkedBinaryTree(20);
        LinkedBinaryTree linkedBinaryTree6 = new LinkedBinaryTree();
        LinkedBinaryTree linkedBinaryTree1 = new LinkedBinaryTree(18, linkedBinaryTree6, linkedBinaryTree2);

        LinkedBinaryTree linkedBinaryTree4 = new LinkedBinaryTree(23);
        LinkedBinaryTree linkedBinaryTree5 = new LinkedBinaryTree(27);
        LinkedBinaryTree linkedBinaryTree3 = new LinkedBinaryTree(152, linkedBinaryTree5, linkedBinaryTree4);

        LinkedBinaryTree linkedBinaryTree = new LinkedBinaryTree(460, linkedBinaryTree1, linkedBinaryTree3);

        Iterator preTree = linkedBinaryTree.iteratorPreOrder();
        Iterator postTree = linkedBinaryTree.iteratorPostOrder();


        System.out.println("输出树");
        System.out.println(linkedBinaryTree.toString());
        System.out.println("输出树根的右子树" + linkedBinaryTree.getRight());
        System.out.println(("输出根的左子树" + linkedBinaryTree.getLeft()));
        System.out.println(("输出树的深度" + linkedBinaryTree.getHeight()));
        System.out.println("是否含有数字460: " + linkedBinaryTree.contains(460));
        System.out.println("前序遍历");
        linkedBinaryTree.toPreString();

        System.out.println("\n后序遍历");
        linkedBinaryTree.toPostString();

    }
}