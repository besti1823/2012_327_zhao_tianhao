package week10;

import java.io.FileNotFoundException;

public class RunDecisionTree {

    public static void main(String[] args) throws FileNotFoundException {
        System.out.println("让我们做一个小测试");
        DecisionTree expert = new DecisionTree("C:\\Users\\12441\\Desktop\\game\\input.txt");
        expert.evaluate();
        System.out.println("打印决策树如下：");
        System.out.println(expert.getTree().toString());
    }
}