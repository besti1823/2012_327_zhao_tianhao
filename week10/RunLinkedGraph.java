package com.company;

public class RunLinkedGraph
{
    public static void main(String[] args)
    {
        LinkedGraph[] peak = new LinkedGraph[5];
        int[][] graph = new int[5][5];
        int[] rudu = new int[5];
        int[] chudu = new int[5];
        for (int i = 0; i < 5;i++)
        {
            for (int n = 0;n < 5;n++)
            {
                graph[i][n] = 0;
            }
            rudu[i] = 0;
            chudu[i] = 0;
        }
        graph[0][1] = 1;  graph[0][2] = 1;  graph[0][3] = 1;
        graph[1][0] = 1;  graph[1][2] = 1;
        graph[2][0] = 1;  graph[2][1] = 1;  graph[2][3] = 1;  graph[2][4] = 1;
        graph[3][0] = 1;  graph[3][2] = 1;  graph[3][4] = 1;
        graph[4][2] = 1;  graph[4][3] = 1;
        System.out.println ("无向图:" );
        for (int i = 0;i < 5;i++)
        {
            for (int n = 0;n < 5;n++)
            {
                System.out.print (graph[i][n] +" ");
                rudu[i] += graph[i][n];
            }
            System.out.println ();
        }
        System.out.print ("度：");
        for (int i = 0;i < 5;i++)
        {
            System.out.print (rudu[i]+" ");
        }
        for (int i = 0;i < 5;i++)
        {
            for (int n = 0;n < 5;n++)
            {
                graph[i][n] = 0;
            }
            rudu[i] = 0;
            chudu[i] = 0;
        }
        graph[0][1]=1;  graph[0][3]=1;
        graph[1][2]=1;  graph[2][0]=1;  graph[2][4]=1;
        graph[3][2]=1;  graph[4][3]=1;
        System.out.println ("\n有向图：" );
        for (int i = 0;i < 5;i++)
        {
            for (int n = 0;n < 5;n++)
            {
                System.out.print (graph[i][n]+" " );
                rudu[i] += graph[i][n];
            }
            System.out.println ();
        }
        System.out.print ("入度：" );
        for (int i = 0;i < 5;i++)
        {
            System.out.print (rudu[i]+" " );
        }
        System.out.print ("\n出度：" );
        for (int i = 0;i < 5;i++)
        {
            for (int n = 0;n < 5;n++)
            {
                chudu[i] += graph[n][i];
            }
            System.out.print (chudu[i]+" " );
        }
        LinkedGraph v03 = new LinkedGraph (peak[3],7);
        LinkedGraph v01 = new LinkedGraph (peak[1],5);
        LinkedGraph v12 = new LinkedGraph (peak[2],4);
        LinkedGraph v24 = new LinkedGraph (peak[4],2);
        LinkedGraph v20 = new LinkedGraph (peak[0],8);
        LinkedGraph v42 = new LinkedGraph (peak[2],6);
        LinkedGraph v53 = new LinkedGraph (peak[3],3);
        System.out.println ("\nV0-->1|5-->3|7" );
        System.out.println ("V1-->2|4" );
        System.out.println ("V2-->0|8-->4|2" );
        System.out.println ("V3-->2|6" );
        System.out.println ("V4-->3|3" );
    }


}