package com.company;

public class LinkedGraph
{
    private int right;
    private LinkedGraph next;
    private LinkedGraph peak;
    public LinkedGraph(LinkedGraph peak,int right)
    {
        this.right = right;
        this.peak = peak;
        next = null;
    }
    public LinkedGraph(LinkedGraph peak,int right, LinkedGraph next)
    {
        this.right = right;
        this.next = next;
        this.peak = peak;
    }
    public int getRight()
    {
        return right;
    }
    public void setRight(int right)
    {
        this.right = right;
    }
    public LinkedGraph getNext()
    {
        return next;
    }
    public void setNext(LinkedGraph next)
    {
        this.next = next;
    }
    public LinkedGraph getPeak()
    {
        return peak;
    }
    public void setPeak(LinkedGraph peak)
    {
        this.peak = peak;
    }
}
