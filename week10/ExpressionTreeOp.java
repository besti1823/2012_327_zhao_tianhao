package week10;

public class ExpressionTreeOp
{
    private int termType;
    private char operator;
    private int value;

    // 使用指定的数据创建一个新的表达式树对象。
    public ExpressionTreeOp(int type, char op, int val)
    {
        termType = type;
        operator = op;
        value = val;
    }

    // 如果此对象是运算符，则返回true，否则返回false。
    public boolean isOperator()
    {
        return (termType == 1);
    }

    // 返回表达式树对象的运算符。
    public char getOperator()
    {
        return operator;
    }

    // 返回表达式树对象的值。
    public int getValue()
    {
        return value;
    }

    public String toString()
    {
        if (termType == 1)
            return operator + "";
        else
            return value + "";
    }
}