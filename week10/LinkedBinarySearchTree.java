package com.company;




public class LinkedBinarySearchTree<T> extends LinkedBinaryTree<T> implements BinarySearchTreeADT<T>
{

    // 创建空的二叉查找树
    public LinkedBinarySearchTree()
    {
        super();
    }

    // 创建二叉查找树用特定根
    public LinkedBinarySearchTree(T element)
    {
        super(element);

        if (!(element instanceof Comparable))
            throw new NonComparableElementException("LinkedBinarySearchTree");
    }

    // 将指定的对象按其自然顺序添加到相应位置的二叉搜索树中。
    // 注意，在右边添加了相等的元素。
    public void addElement(T element)
    {
        if (!(element instanceof Comparable))
            throw new NonComparableElementException("LinkedBinarySearchTree");

        Comparable<T> comparableElement = (Comparable<T>)element;

        if (isEmpty())
            root = new BinaryTreeNode<T>(element);
        else
        {
            if (comparableElement.compareTo(root.getElement()) < 0)
            {
                if (root.getLeft() == null)
                    this.getRootNode().setLeft(new BinaryTreeNode<T>(element));
                else
                    addElement(element, root.getLeft());
            }
            else
            {
                if (root.getRight() == null)
                    this.getRootNode().setRight(new BinaryTreeNode<T>(element));
                else
                    addElement(element, root.getRight());
            }
        }
        modCount++;
    }

    // 将指定的对象按其自然顺序添加到相应位置的二叉搜索树中。
    // 注意，在右边添加了相等的元素。
    private void addElement(T element, BinaryTreeNode<T> node)
    {
        Comparable<T> comparableElement = (Comparable<T>)element;

        if (comparableElement.compareTo(node.getElement()) < 0)
        {
            if (node.getLeft() == null)
                node.setLeft(new BinaryTreeNode<T>(element));
            else
                addElement(element, node.getLeft());
        }
        else
        {
            if (node.getRight() == null)
                node.setRight(new BinaryTreeNode<T>(element));
            else
                addElement(element, node.getRight());
        }
    }


    // 从二叉搜索树中删除与指定目标元素匹配的第一个元素，并返回对该元素的引用。
    // 如果在二叉搜索树中没有找到指定的目标元素，则抛出ElementNotFoundException。
    public T removeElement(T targetElement)
            throws ElementNotFoundException
    {
        T result = null;

        if (isEmpty())
            throw new ElementNotFoundException("LinkedBinarySearchTree");
        else
        {
            BinaryTreeNode<T> parent = null;
            if (((Comparable<T>)targetElement).equals(root.element))
            {
                result =  root.element;
                BinaryTreeNode<T> temp = replacement(root);
                if (temp == null)
                    root = null;
                else
                {
                    root.element = temp.element;
                    root.setRight(temp.right);
                    root.setLeft(temp.left);
                }

                modCount--;
            }
            else
            {
                parent = root;
                if (((Comparable)targetElement).compareTo(root.element) < 0)
                    result = removeElement(targetElement, root.getLeft(), parent);
                else
                    result = removeElement(targetElement, root.getRight(), parent);
            }
        }

        return result;
    }


    private T removeElement(T targetElement, BinaryTreeNode<T> node, BinaryTreeNode<T> parent)
            throws ElementNotFoundException
    {
        T result = null;

        if (node == null)
            throw new ElementNotFoundException("LinkedBinarySearchTree");
        else
        {
            if (((Comparable<T>)targetElement).equals(node.element))
            {
                result =  node.element;
                BinaryTreeNode<T> temp = replacement(node);
                if (parent.right == node)
                    parent.right = temp;
                else
                    parent.left = temp;

                modCount--;
            }
            else
            {
                parent = node;
                if (((Comparable)targetElement).compareTo(node.element) < 0)
                    result = removeElement(targetElement, node.getLeft(), parent);
                else
                    result = removeElement(targetElement, node.getRight(), parent);
            }
        }

        return result;
    }

    // 返回对节点的引用，该节点将替换指定要删除的节点。
    // 在删除的节点有两个子节点的情况下，将使用中序继承节点作为其替代节点。
    private BinaryTreeNode<T> replacement(BinaryTreeNode<T> node)
    {
        BinaryTreeNode<T> result = null;

        if ((node.left == null) && (node.right == null))
            result = null;

        else if ((node.left != null) && (node.right == null))
            result = node.left;

        else if ((node.left == null) && (node.right != null))
            result = node.right;

        else
        {
            BinaryTreeNode<T> current = node.right;
            BinaryTreeNode<T> parent = node;

            while (current.left != null)
            {
                parent = current;
                current = current.left;
            }

            current.left = node.left;
            if (node.right != current)
            {
                parent.left = current.right;
                current.right = node.right;
            }

            result = current;
        }

        return result;
    }


    public void removeAllOccurrences(T targetElement)
            throws ElementNotFoundException
    {
        removeElement(targetElement);

        try
        {
            while (contains((T)targetElement))
                removeElement(targetElement);
        }

        catch (Exception ElementNotFoundException)
        {
        }
    }


    public T removeMin() throws EmptyCollectionException
    {
        T result = null;

        if (isEmpty())
            throw new EmptyCollectionException("LinkedBinarySearchTree");
        else
        {
            if (root.left == null)
            {
                result = root.element;
                root = root.right;
            }
            else
            {
                BinaryTreeNode<T> parent = root;
                BinaryTreeNode<T> current = root.left;
                while (current.left != null)
                {
                    parent = current;
                    current = current.left;
                }
                result =  current.element;
                parent.left = current.right;
            }

            modCount--;
        }

        return result;
    }


    public T removeMax() throws EmptyCollectionException
    {
        T result = null;

        if (isEmpty())
        {
            throw new EmptyCollectionException("LinkedBinarySearchTree");
        }
        else
        {
            if (root.right == null)
            {
                result = root.element;
            }
            else
            {
                BinaryTreeNode<T> parent = root;
                BinaryTreeNode<T> current = root.right;
                while (current.right != null)
                {
                    parent = current;
                    current = current.right;
                }
                result =  current.element;
                parent.right = current.left;
            }
            modCount --;
        }
        return result;
    }


    public T findMin() throws EmptyCollectionException
    {
        T result = null;

        if (isEmpty())
            throw new EmptyCollectionException("LinkedBinarySearchTree");
        else
        {
            if (root.left == null)
            {
                result = root.element;
                root = root.right;
            }
            else
            {
                BinaryTreeNode<T> current = root.left;
                while (current.left != null)
                {
                    current = current.left;
                }
                result =  current.element;
            }
        }
        return result;
    }


    public T findMax() throws EmptyCollectionException
    {
        T result = null;

        if (isEmpty())
        {
            throw new EmptyCollectionException("LinkedBinarySearchTree");
        }
        else
        {
            if (root.right == null)
            {
                result = root.element;
            }
            else
            {
                BinaryTreeNode<T> current = root.right;
                while (current.right != null)
                {
                    current = current.right;
                }
                result =  current.element;
            }
        }
        return result;
    }


    public T find(T targetElement) throws ElementNotFoundException
    {
        return null;
    }


    public LinkedBinarySearchTree<T> getLeft()
    {
        return (LinkedBinarySearchTree<T>) left;
    }


    public LinkedBinarySearchTree<T> getRight()
    {
        return (LinkedBinarySearchTree<T>) right;
    }


    private BinaryTreeNode<T> findNode(T targetElement, BinaryTreeNode<T> next)
    {
        return null;
    }

    public String toString()
    {
        return super.toString();
    }
}