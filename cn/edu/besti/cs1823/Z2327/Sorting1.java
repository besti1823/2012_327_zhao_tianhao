package cn.edu.besti.cs1823.Z2327;

import java.util.Iterator;

import jdk.ArrayHeap;
import jdk.LinkedBinarySearchTree;
import jdk.ArrayBinaryTree;
public class Sorting1

{

    //使用所选内容对指定的整数数组进行排序排序算法。

    public static <T extends Comparable<T>>

    void selectionSort(T[] data)

    {
        int min;
        T temp;
        for (int index = 0; index < data.length-1; index++)

        {

            min = index;
            for (int scan = index+1; scan < data.length; scan++)
                if (data[scan].compareTo(data[min])<0)
                    min = scan;
            swap(data, min, index);
        }

    }

    //交换到数组中的元素。由各种排序算法使用。

    private static <T extends Comparable<T>>

    void swap(T[] data, int index1, int index2)

    {

        T temp = data[index1];
        data[index1] = data[index2];
        data[index2] = temp;
    }

    //插入排序
    public static <T extends Comparable<T>> void insertionSort(T[] data)
    {
        for (int index = 1; index < data.length; index++)
        {
            T key = data[index];
            int position = index;

            while (position > 0 && data[position-1].compareTo(key) > 0)

            {

                data[position] = data[position-1];
                position--;

            }

            data[position] = key;
        }

    }

    //冒泡排序
    public static <T extends Comparable<T>>
    void bubbleSort(T[] data)
    {
        int position, scan;
        T temp;
        for (position =  data.length - 1; position >= 0; position--)
        {
            for (scan = 0; scan <= position - 1; scan++)
            {
                if (data[scan].compareTo(data[scan+1]) > 0)
                    swap(data, scan, scan + 1);
            }


        }


    }


    //快速排序

    public static <T extends Comparable<T>> void quickSort(T[] data)

    {


        quickSort(data, 0, data.length - 1);


    }


    private static <T extends Comparable<T>>

    void quickSort(T[] data, int min, int max)

    {

        if (min < max)


        {

            int indexofpartition = partition(data, min, max);
            quickSort(data, min, indexofpartition - 1);
            quickSort(data, indexofpartition + 1, max);
        }


    }

    private static <T extends Comparable<T>>
    int partition(T[] data, int min, int max)

    {


        T partitionelement;


        int left, right;


        int middle = (min + max) / 2;

        partitionelement = data[middle];


        // move it out of the way for now


        swap(data, middle, min);





        left = min;


        right = max;





        while (left < right)


        {


            // search for an element that is > the partition element


            while (left < right && data[left].compareTo(partitionelement) <= 0)


                left++;





            // search for an element that is < the partition element


            while (data[right].compareTo(partitionelement) > 0)


                right--;





            // swap the elements


            if (left < right)


                swap(data, left, right);


        }

        swap(data, min, right);
        return right;

    }

    //希尔排序
    public static <T extends Comparable<T>> void shellsort(T[] DATA){

        for (int gap=DATA.length/2;gap>0;gap=gap/2){


            for (int n = gap;n<DATA.length;n++){


                int i=n;


                while (i-gap>=0&&DATA[i].compareTo(DATA[i-gap])<0){


                    swap(DATA,i,i-gap);


                    i-=gap;


                }


            }


        }


    }


    //堆排序


    public static <T extends Comparable<T>>void heapSort(T[] data)


    {


        ArrayHeap<T> ArrayHeap = new ArrayHeap<>();


        for (int n=0;n<data.length;n++) {


            ArrayHeap.addElement(data[n]);


        }


        for (int m=0;m<data.length;m++) {


            data[m] = ArrayHeap.removeMin();


        }


    }


    //二叉树排序


    public static <T extends Comparable<T>>void binaryTreeSort(T[] data)


    {


        LinkedBinarySearchTree<T> linkedBinarySearchTree = new LinkedBinarySearchTree<>();


        for (int i=0;i<data.length;i++) {


            linkedBinarySearchTree.addElement(data[i]);


        }


        for (int j = 0; j < data.length; j++) {


            data[j] = linkedBinarySearchTree.removeMin();


        }


    }



}