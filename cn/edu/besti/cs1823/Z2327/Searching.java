package cn.edu.besti.is.exp75;

public class Searching {
    public Searching()
    {

    }
    public  boolean linearSearch(String a, int target)
    {
        String[] s = a.split("\\s");
        int data[]=new int[s.length];
        for(int i=0;i<s.length;i++)
        {
            int num = Integer.parseInt(s[i]);
            data[i] = num;
        }
        for(int i = 0;i<data.length;i++)
        {
            if(data[i]==target)
                return true;
        }
        return false;
    }
    public  boolean BinarySearch2(int a[], int value, int low, int high)
    {
        int mid = low+(high-low)/2;
        if(a[mid]==value)
            return true;
        if(a[mid]>value)
            return BinarySearch2(a, value, low, mid-1);
        if(a[mid]<value)

            return BinarySearch2(a, value, mid+1, high);
        return false;

    }
    public int InsertionSearch(int a[], int value, int low, int high)
    {
        int mid = low+(value-a[low])/(a[high]-a[low])*(high-low);
        if(a[mid]==value)
            return mid;
        if(a[mid]>value)
            return InsertionSearch(a, value, low, mid-1);
        if(a[mid]<value)
            return InsertionSearch(a, value, mid+1, high);
        return 0;

    }

    public static boolean FibonacciSearch(int[] table, int keyWord) {

        int i = 0;
        while (getFibonacci(i) - 1 == table.length) {
            i++;
        }

        int low = 0;
        int height = table.length - 1;
        while (low <= height) {
            int mid = low + getFibonacci(i - 1);
            if (table[mid] == keyWord) {
                return true;
            } else if (table[mid] > keyWord) {
                height = mid - 1;
                i--;
            } else if (table[mid] < keyWord) {
                low = mid + 1;
                i -= 2;
            }
        }
        return false;
    }

    public static int getFibonacci(int n) {
        int res = 0;
        if (n == 0) {
            res = 0;
        } else if (n == 1) {
            res = 1;
        } else {
            int first = 0;
            int second = 1;
            for (int i = 2; i <= n; i++) {
                res = first + second;
                first = second;
                second = res;
            }
        }
        return res;
    }
    public static  boolean erchashu(Comparebale tree, Comparebale target)
    {
        while (tree!=null)
        {
            if(tree.geti()<target.geti())
            {
                tree=tree.next;
            }
            else if (tree.geti()>target.geti())
            {
                tree = tree.secondnext;
            }
            else if(tree.geti()==target.geti())
                return true;
        }
        return false;
    }
    public static  boolean lianbiao(Comparebale[] date,Comparebale target)
    {
        int num = target.geti()%11;
        boolean i;
        if(date[num].geti()==-1)
        {
            return  false;
        }
        else
        {
            while (date[num].geti()!=target.geti())
            {
                date[num]=date[num].next;
                if(date[num]==null)
                    return false;
            }
            return  true;
        }
    }
    public int[] hash(int[] arr){
        int[] arr1 = {0,0,0,0,0,0,0,0,0,0,0,0};
        for(int i=0;i<arr.length;i++)
        {
            if(arr1[arr[i]%11] == 0)
                arr1[arr[i]%11] = arr[i];
            else
            {
                for(int j=2;j<arr.length;j++)
                    if(arr1[j-1] == 0)
                    {
                        arr1[j-1] = arr[i];
                        break;
                    }
            }
        }
        return arr1;
    }

    public int hashsearch(int[] result,int target){
        int k = target%11,i,re = 0;
        if(result[k]==target)
            re =  result[k];
        else
        {
            for(i=k;k<result.length;k++)
            {
                if(result[k]==target)
                {
                    re = result[k];
                    break;
                }
            }
        }
        return re;
    }
    public static boolean blooksearch(int[] a ,int key)
    {
        int[] b = new int[3];
        int n = a.length/3;
        int max = a[0];
        int j=0;
        while (n<=9)
        {
            for(int i=n-3;i<n;i++)
            {
                if(max<a[i])
                    max = a[i];
            }
            b[j]=max;
            j++;
            n=n+3;
        }
        for(int i=0;i<b.length;i++)
        {
            if(b[i]>=key)
            {
                for(int l = (i+1)*3-1;l>=(i+1)*3-3;l--)
                {
                    if(a[l]==key)
                        return true;
                }
                return  false;
            }

        }
        return false;
    }
}