package cn.edu.besti.is.exp75;

public class Comparebale {
    private int i=-1;
    public Comparebale next=null;
    public Comparebale secondnext=null;
    public Comparebale(int i)
    {
        this.i=i;
    }
    public  int compareto(Comparebale a)
    {
        int result = i-a.geti();
        return result;
    }
    public int geti()
    {
        return  i;
    }
    public void setI(int i)
    {
        this.i=i;
    }

    @Override
    public String toString()
    {
        String string = i+"";
        return string;
    }
    public void setNext(Comparebale a)
    {
        next = a;
    }
    public void setSecondnext(Comparebale b)
    {
        secondnext = b;
    }
    public Comparebale getNext()
    {
        return  next;
    }
    public Comparebale getSecondnext()
    {
        return  secondnext;
    }
}