package cn.edu.besti.cs1823.Z2327;

public class Searching1
{
//线性查找
    public static Comparable linearSearch (Comparable[] data,
                                           Comparable target)
    {
        Comparable result = null;
        int index = 0;
        while (result == null && index < data.length)
        {
            if (data[index].compareTo(target) == 0)
                result = data[index];
            index++;
        }
        return result;

    }
//折半查找
    public static Comparable binarySearch (Comparable[] data, Comparable target)
    {
        Comparable result = null;
        int first = 0, last = data.length-1, mid;
        while (result == null && first <= last)
        {
            mid = (first + last) / 2;  // determine midpoint
            if (data[mid].compareTo(target) == 0)
                result = data[mid];
            else
            if (data[mid].compareTo(target) > 0)
                last = mid - 1;
            else
                first = mid + 1;
        }

        return result;

    }

    //顺序查找
    public static Comparable sequenceSearch(Comparable a[], Comparable value, int n) {
        for (int i = 0; i < n; i++)
            if (a[i].compareTo(value) == 0)


                return i;


        return -1;


    }
    //插值查找

    public static int InsertionSearch(int[] a, int value, int low, int high) {

        int mid = low + (value - a[low]) / (a[high] - a[low]) * (high - low);
        if (a[mid] == value)
            return mid;
        if (a[mid] > value)
            return InsertionSearch(a, value, low, mid - 1);
        else
            return InsertionSearch(a, value, mid + 1, high);

    }}