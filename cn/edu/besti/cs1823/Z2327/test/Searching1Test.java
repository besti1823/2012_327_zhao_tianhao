package cn.edu.besti.cs1823.Z2327.test;

import cn.edu.besti.cs1823.Z2327.Searching1;
import org.junit.Test;
import static org.junit.Assert.*;

public class Searching1Test {

    @Test

    public void linearSearch() {

        Comparable[] ARR1 = new Comparable[]{20, 18,23, 27, 2, 19, 11, 2, 2327};
        assertEquals(2327, Searching1.linearSearch(ARR1,  2327));
        assertNotEquals(30, Searching1.linearSearch(ARR1,  30));


    }



    @Test

    public void binarySearch() {

        Comparable[] ARR2 = new Comparable[]{20, 18,23, 27, 2, 19, 11, 2, 2327};

        assertEquals(2327, Searching1.linearSearch(ARR2,  2327));

        assertNotEquals(31, Searching1.linearSearch(ARR2,  31));

    }

    @Test

    public void sequenceSearch() {

        Comparable[] ARR3 = new Comparable[]{20, 18,23, 27, 2, 19, 11, 2, 2327};

        assertEquals(2327, Searching1.linearSearch(ARR3,  2327));


        assertNotEquals(32, Searching1.linearSearch(ARR3,  32));


    }



    @Test

    public void insertionSearch() {

        Comparable[] ARR4 = new Comparable[]{20, 18,23, 27, 2, 19, 11, 2, 2327};

        assertEquals(2327, Searching1.linearSearch(ARR4,  2327));

        assertNotEquals(33, Searching1.linearSearch(ARR4,  33));


    }



}