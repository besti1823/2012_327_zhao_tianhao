package cn.edu.besti.is.exp75;

import android.os.Bundle;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    AutoCompleteTextView s1;
    AutoCompleteTextView s2;
    AutoCompleteTextView s3;
    AutoCompleteTextView s4;
    Button a1;
    Button a2;
    Button a3;
    Button a4;
    Button a5;
    Button a6;
    Button a7;
    Button a8;
    Button a9;
    Button a10;
    Searching searching = new Searching();
    sort s = new sort();
    Node n = new Node();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        s1 = findViewById(R.id.autoCompleteTextView);
        s2 = findViewById(R.id.autoCompleteTextView2);
        s3 = findViewById(R.id.autoCompleteTextView3);
        s4= findViewById(R.id.autoCompleteTextView4);
        a1 = findViewById(R.id.button4);
        a2 = findViewById(R.id.button5);
        a3= findViewById(R.id.button6);
        a4 = findViewById(R.id.button7);
        a5 = findViewById(R.id.button8);
        a6 = findViewById(R.id.button9);
        a7 = findViewById(R.id.button10);
        a8 =findViewById(R.id.button11);
        a9 = findViewById(R.id.button12);
        a10 = findViewById(R.id.button13);
        a1.setOnClickListener(xuanze);
        a2.setOnClickListener(maopao);
        a3.setOnClickListener(charu);
        a4.setOnClickListener(xier);
        a5.setOnClickListener(dui);
        a6.setOnClickListener(shunxu);
        a7.setOnClickListener(erfen);
        a8.setOnClickListener(fenbu);
        a9.setOnClickListener(haxi);
        a10.setOnClickListener(feinaboqie);

    }

    View.OnClickListener xuanze = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String b = s1.getText().toString();
            String result =s.selectionsort(b);
            s2.setText(result);
        }
    };

    View.OnClickListener maopao = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String b =s1.getText().toString();
            String result = s.maopao(b);
            s2.setText(result);
        }
    };
    View.OnClickListener charu = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String b = s1.getText().toString();
            String[] s = b.split("\\s");
            for(int i=0;i<s.length;i++)
            {
                int num = Integer.parseInt(s[i]);
                n.add(num);
            }
            s2.setText(n.values()+"");
        }
    };
    View.OnClickListener xier = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String b =s1.getText().toString();
            String[] a = b.split("\\s");
            int[] c = new int[a.length];
            for(int i=0;i<a.length;i++)
            {
                int num = Integer.parseInt(a[i]);
                c[i]=num;
            }
            String result = s.sort(c);
            s2.setText(result);

        }
    };
    View.OnClickListener dui = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String b =s1.getText().toString();
            String[] a = b.split("\\s");

            int[] c = new int[a.length];
            for(int i=0;i<a.length;i++)
            {
                int num = Integer.parseInt(a[i]);
                c[i]=num;
            }
            String result = s.dui(c);
            s2.setText(result);
        }
    };
    View.OnClickListener shunxu = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String b = s1.getText().toString();
            int target =Integer.parseInt( s3.getText().toString());
            boolean i = searching.linearSearch(b,target);
            s4.setText(i+"");
        }
    };
    View.OnClickListener erfen = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String b = s1.getText().toString();
            String[] s9 = b.split("\\s");
            int target =Integer.parseInt( s3.getText().toString());
            int[] c = new int[s9.length];
            for(int i=0;i<s9.length;i++)
            {
                int num = Integer.parseInt(s9[i]);
                c[i]=num;
            }
            boolean i = searching.BinarySearch2(c,target,0,c.length);
            s4.setText(i+" ");
        }
    };
    View.OnClickListener fenbu =  new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String b = s1.getText().toString();
            String[] s9 = b.split("\\s");
            int target =Integer.parseInt( s3.getText().toString());
            int[] c = new int[s9.length];
            for(int i=0;i<s9.length;i++)
            {
                int num = Integer.parseInt(s9[i]);
                c[i]=num;
            }
            boolean i = searching.blooksearch(c,target);
            s4.setText(i+" ");
        }
    };
    View.OnClickListener haxi = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String b1 =s3.getText().toString();
            int target = Integer.parseInt(b1);
            String b = s1.getText().toString();
            String[] c = b.split(" ");
            int[] d = new int[c.length];
            for(int i=0;i<c.length;i++)
            {
                int num = Integer.parseInt(c[i]);
                d[i] = num;
            }
            int result[] = searching.hash(d);
            String re = searching.hashsearch(result,target)+"";
            s4.setText(re);
        }
    };
    View.OnClickListener feinaboqie = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String b1 =s3.getText().toString();
            int target = Integer.parseInt(b1);
            String b = s1.getText().toString();
            String[] c = b.split(" ");
            int[] d = new int[c.length];
            for(int i=0;i<c.length;i++)
            {
                int num = Integer.parseInt(c[i]);
                d[i] = num;
            }
            String re = searching.FibonacciSearch(d,target)+"";
            s4.setText(re);
        }
    };
}