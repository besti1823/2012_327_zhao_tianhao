package week7;

public interface QueueADT<T> {
    /*往队列的前端添加一个元素*/
    public void enqueueHead(T element);
    /*往队列的后端添加一个元素*/
    public void enqueueLast(T element);
    /*从队列中前端移除一个元素*/
    public T dequeueHead();
    /*从队列中末端移除一个元素*/
    public T dequeueLast();

    /*考察队列前端的那个元素 */
    public T first();
    /*考察队列末端的那个元素 */
    public T Last();


    /*判定队列是否为空*/
    public boolean isEmpty();
    /*判定队列中的元素数目*/
    public int size();
    /*返回队列的字符串表示*/
    public String  toString();



}