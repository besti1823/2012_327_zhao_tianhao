package week7;

import jdk.EmptyCollectionException;

public class LinkedQueue<T> implements QueueADT<T>
{
    private int count;
    private LinearNode<T> head,tail;


    public LinkedQueue()
    {
        count=0;
        head = tail =null;
    }

    public void enqueue(T element)
    {
        LinearNode<T> node = new LinearNode<T>(element);

        if(isEmpty())
            head = node;
        else
            tail.setNext(node);
        tail=node;
        count++;
    }

    public T dequeue() throws EmptyCollectionException
    {
        if(isEmpty())
            throw new EmptyCollectionException("queue");

        T result = head.getElement();
        head = head.getNext();
        count--;

        if(isEmpty())
            tail = null;

        return result;
    }

    @Override
    public void enqueueHead(T element) {

    }

    @Override
    public void enqueueLast(T element) {

    }

    @Override
    public T dequeueHead() {
        return null;
    }

    @Override
    public T dequeueLast() {
        return null;
    }

    @Override
    public T first() {
        return head.getElement();
    }

    @Override
    public T Last() {
        return null;
    }

    @Override
    public boolean isEmpty() {
        if(count!=0)
            return false;
        else
            return true;
    }

    @Override
    public int size() {
        return count;
    }

    public String toString()
    {
        String result = "";
        for(int a = 0;a<count;a++)
        {
            result += head.getElement();
            head = head.getNext();
        }
        return result;
    }
}