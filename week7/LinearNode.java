package week7;

public class LinearNode<T> {
    private LinearNode<T> next, previous;
    private T element;

    public LinearNode(){
        next = null;
        element = null;
        previous = null;
    }

    public LinearNode(T elem){
        next = null;
        element = elem;
    }

    public LinearNode<T> getNext(){
        return next;
    }

    public void setNext(LinearNode<T> node){
        next = node;
    }

    public LinearNode<T> getPrevious(){
        return previous;
    }

    public void setPrevious(LinearNode<T> node){
        previous = node;
    }

    public T getElement(){
        return element ;
    }

    public void setElement(T elem){
        element = elem;
    }
}