package week7;

public interface StackADTpp141<T> {


    public void push(T element);


    public T pop();


    public T peek();


    public boolean isEmpty();


    public int size();


    public String toString();


}