package cn.edu.besti.is.layout;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.layout.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}

