/*************************************************************************
    > File Name: JISUANQI.java
    > Author: Tokisaki Krumi
    > Mail: 1244122913@qq.com 
    > Created Time: 2019年09月15日 星期日 18时52分34秒
 ************************************************************************/
import java.util.Scanner;
public class JISUANQI {
	public static double JISUANQI(int a,char op,int b){
		int c;
		double answer=0;
		switch (op)
		{
			case '+':
				answer = a + b;
				break;
			case '-':
				answer = a - b;
				break;
			case '*':
				answer = a * b;
				break;
			case '/':
				if( b==0 ) System.out.println("第二个数不能为0！！");
				answer = a /(float) b;
				break;
			case'%':
				answer = a % b;
				break;
			default:System.out.println("错误！！请检查并重新输入！");	
		}
		return answer;
	}
}
