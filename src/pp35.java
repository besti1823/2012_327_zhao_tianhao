/*************************************************************************
    > File Name: pp35.java
    > Author: Tokisaki Krumi
    > Mail: 1244122913@qq.com 
    > Created Time: 2019年09月21日 星期六 22时04分41秒
 ************************************************************************/
import java.util.*;
public class pp35
{

	  
	public static void main(String[] args)
		     
	{

				     
		Scanner scan = new Scanner(System.in);
					     
		System.out.println("请输入球的半径");
						      
		double r = scan.nextDouble();
							     
		double s,v;
								      
		s = 4*Math.PI*r*r;
									     
		v = 4*Math.PI*r*r*r/3;
										      
		System.out.println("球的表面积是："+s+"\n球的体积是："+v);
											     }
}
