/*************************************************************************
    > File Name: pp210.java
    > Author: Tokisaki Krumi
    > Mail: 1244122913@qq.com 
    > Created Time: 2019年09月21日 星期六 18时05分49秒
 ************************************************************************/
import java.util.Scanner;
public class pp210{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		int c;
		int s;
		System.out.println("请输入边长：");
		int a = sc.nextInt();
		c = a*4;
		s = a*a;
		System.out.println("正方形周长为："+c);
		System.out.println("正方形面积为："+s);
	}
}
