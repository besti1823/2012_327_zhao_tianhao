/*************************************************************************
    > File Name: JiSuan.java
    > Author: Tokisaki Krumi
    > Mail: 1244122913@qq.com 
    > Created Time: 2019年09月11日 星期三 09时11分12秒
 ************************************************************************/
import java.util.Scanner;
public class JiSuan{
	public static void main (String[] args){
		Scanner scan = new Scanner (System.in);
		System.out.println("请输入第一个数：");
		int a = scan.nextInt () ;
		System.out.println("请输入第二个数：");
		int b = scan.nextInt () ;
		System.out.println("a="+a);
		System.out.println("b="+b);
		System.out.println("a+b="+(a+b));
		System.out.println("a-b="+(a-b));
		System.out.println("a*b="+(a*b));
		System.out.println("a/b="+(a/b));

	}
}
