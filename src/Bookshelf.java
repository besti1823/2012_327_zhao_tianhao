/*************************************************************************
    > File Name: Bookshelf.java
    > Author: Tokisaki Krumi
    > Mail: 1244122913@qq.com 
    > Created Time: 2019年09月20日 星期五 21时57分15秒
 ************************************************************************/
import java.util.Scanner;

class Book
 {


	         private String bookname;
			         
			 private String author;
					         
			 private String press;
							         
			 public String copyrightdate;

									        
			 public Book(String bookname,String author,String press,String copyrightdate)
												         
			 {

															           
															 
				 this.bookname = bookname;// 录入书名
																			
				 this.author = author;// 录入作者
																					
				 this.press = press;// 录入出版社
																							
				 this.copyrightdate = copyrightdate;// 录入版权
																							        }
											       
			     public void setBookname(String t)
														        
				 {

																			
					 bookname = t;
																			
				 }

													
				 public String getBookname() {

																	
					 return bookname;
																	
				 }

														
				 public void setAuthor(String a) {

																	
					 author = a;

																		
				 }

															
				 public String getAuthor() {

																		
					 return author;

																				
				 }

																
				 public void setPress(String p) {

																				
					 press = p;
																				
				 }

																		
				 public String getPress() {

																				
					 return press;

																					
				 }

																			
				 public void setCopyrightdate(String d) {

																						
					 copyrightdate = d;
																						
				 }

																			
				 public String getCopyrightdate() {

																						
					 return copyrightdate;
																							
				 }

																				
				 public String toString() {

																							
					 return "bookname:" + this.bookname + ",author:" + this.author + ",press:" + this.press + ",copyrightdate:"
																												
						 + this.copyrightdate;
																								
				 }
																					
																					
																															
				 }
																						
																




public class Bookshelf{

	   public static void main (String []args){

		         Book book = new Book("The Garden Of Sinners\n","Naoko Mushroom\n","Publishing House of Type--Moon\n","1998-5\n");
				       System.out.println(book);
								       }
}
																						
