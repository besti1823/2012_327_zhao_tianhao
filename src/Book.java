/*************************************************************************
    > File Name: Book.java
    > Author: Tokisaki Krumi
    > Mail: 1244122913@qq.com 
    > Created Time: 2019年09月20日 星期五 17时19分23秒
 ************************************************************************/
public class Book {


	    // 设置属性的私有访问权限
	     private String name;
	       
		 private String author;
		            
		 private String press;
		                 
		 private String copyrightdate;
		    
		 // 通过公有的get,set方法实现对属性的访问
		                         
		                                        
		 public void setname(String name) {
		                                               
			 this.name = name;
		                                                    
		 }

		 public String getname(){
			 return name;
		 }
	                                                      
		
		 
		 
		 
		 public String getauthor() {
		                                                             
			 return author;
		                                                                  
		 }
		                                                                       
		 public void setauthor(String author) {
		                                                                                
			 this.author = author;
	                                                                                   
		 }
		                                                                                
		 public String getpress() {
		                                                                                                
			 return press;
		                                                                                                   
		 }
		                                                                                                       
		 public void setpress(String press) {
		
	                                                                                                                
			 this.press =press;
	                                                                                                                   
		 }
	                                                                                                                        
		 public String getcopyrightdate() {
	                                                                                                                                 
			 return copyrightdate;
	                                                                                                                                
		 }
	                                                                                                                                       
		                                                                                                                                                                                          
		 // 有参构造方法 实现属性赋值
		                                                                                                                                                                                                               
		 public Book(String name, String author, String press,String copyrightdate) {
		 
	                                                                                                                                                                                                                        
			 this.setname(name);
	                                                                                                                                                                                                                                  
			 this.setauthor(author);
	                                                                                                                                                                                                                                         
			 this.setpress(press);
		                                                                                                                                                                                                                                                 
			 this.setcopyrightdate(copyrightdate);
		
		 }
		
		 public String toString()
	       {

			         return "name:"+this.name()+"\n"+"author:"+this.getauthor()+"\n"+"press:"+this.getpress()+"\n"+"copyrightdate:"+this.getcopyrightdate();
						        }
		   }
		
	                 

		
	                                                                                                                                                                                                                                                                                            
	

