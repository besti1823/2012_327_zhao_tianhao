/*************************************************************************
    > File Name: pp26.java
    > Author: Tokisaki Krumi
    > Mail: 1244122913@qq.com 
    > Created Time: 2019年09月21日 星期六 17时19分48秒
 ************************************************************************/
import java.util.Scanner;									  
public class pp26{
	public static void main(String [] args){

		Scanner sc = new Scanner(System.in);

		int a;

		System.out.println("输入小时：");
		int h = sc.nextInt();
		System.out.println("输入分钟:" );
		int m = sc.nextInt();
		System.out.println("输入秒：");
		int s = sc.nextInt();

		a = h*3600 + m*60 + s;
		System.out.printf("转换为秒："+a);
		
	}
}
